package fr.braindot.race

import fr.braindot.race.ChannelRace.Companion.asRace
import fr.braindot.race.ChannelRace.Companion.raceFrom
import fr.braindot.race.ChannelRace.Companion.toChannelRace
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch

/**
 * Implementation of a [HotRace] using Kotlin's [Channel].
 * This implementation only allows the consumption to happen once.
 *
 * Use [asRace] or [raceFrom] to get an instance of this class.
 *
 * The [parallelism] attribute is used to define how the class behaves:
 * it defines the number of coroutines [launched][launch] on the [scope] for each operation.
 * Even if it is set to 1, multiple operations may still run concurrently from each other.
 * When set to an number other than 1, it is possible that this class generates an additional
 * coroutine to await the others and close the upstream channel.
 *
 * The [capacity] attribute represents the maximum number of elements stored in an intermediary [Channel].
 * As such, the maximum number of elements at any point of the pipeline is
 * `parallelism + capacity` (`capacity` elements in the channel of that
 * operation to the next, and `parallelism` elements stuck in jobs of the previous
 * stage that cannot start working on their next element).
 */
class ChannelRace<T> private constructor(
	@Suppress("MemberVisibilityCanBePrivate")
	val scope: CoroutineScope,

	private val _upstream: ReceiveChannel<T>,

	@Suppress("MemberVisibilityCanBePrivate")
	val parallelism: Int,

	private val capacity: Int
) : HotRace<T> {

	//region Parameter validation

	init {
		require(parallelism >= 1) { "The number of actors working in parallel must be at least 1, found: $parallelism" }
		require(capacity >= 1) { "The capacity must be at least 1, found: $capacity" }
	}

	//endregion
	//region Single consumption

	private var consumed = false

	private val consumeUpstream: ReceiveChannel<T>
		get() {
			consume()
			return _upstream
		}

	private fun consume() {
		check(!consumed) { "This race has already been consumed, it is not possible to consume it again." }
		consumed = true
	}

	//endregion
	//region Implementation

	/**
	 * Executes [block] on all elements of this race, and suspends until they haven't all finished.
	 *
	 * [block] is executed in [scope], split in [n][parallelism] coroutines.
	 */
	override suspend fun forEach(block: suspend (T) -> Unit) {
		if (parallelism == 1)
			return forEachSequential(block)

		val jobs = ArrayList<Job>()

		val upstream = consumeUpstream
		for (i in 0 until parallelism) {
			jobs += scope.launch {
				for (it in upstream)
					block(it)
			}
		}

		jobs.joinAll()
	}

	override suspend fun forEachSequential(block: suspend (T) -> Unit) {
		for (it in consumeUpstream)
			block(it)
	}

	/**
	 * Maps a [ReceiveChannel] to a [SendChannel] by executing [operations] from the first one and sending the results to the send one.
	 *
	 * Respects the [parallelism] setting.
	 */
	private fun <U> ReceiveChannel<T>.actor(operations: suspend (ReceiveChannel<T>, SendChannel<U>) -> Unit): ReceiveChannel<U> {
		val upstream = this
		val downstream = Channel<U>(capacity = capacity)

		if (parallelism == 1) {
			scope.launch {
				operations(upstream, downstream)
				downstream.close()
			}
		} else {
			val jobs = ArrayList<Job>()

			for (i in 0 until parallelism) {
				jobs += scope.launch {
					operations(upstream, downstream)
				}
			}

			scope.launch {
				jobs.joinAll()
				downstream.close()
			}
		}

		return downstream
	}

	private fun <U> ReceiveChannel<T>.actorMap(transform: suspend (T) -> U) =
		actor<U> { input, output ->
			for (i in input)
				output.send(transform(i))
		}

	override fun <U> map(transform: suspend (T) -> U): ChannelRace<U> =
		ChannelRace(scope, consumeUpstream.actorMap(transform), parallelism, capacity)

	override fun filter(predicate: suspend (T) -> Boolean): ChannelRace<T> =
		consumeUpstream.actor<T> { input, output ->
			for (i in input)
				if (predicate(i))
					output.send(i)
		}.asRace(scope, parallelism, capacity)

	//endregion
	//region Controls

	/**
	 * Run all subsequent operations in [n] coroutines (per operation).
	 * For some operations, an additional coroutine might be required to close the input [Channel].
	 *
	 * @see parallelism
	 * @see sequential
	 */
	fun parallel(n: Int) = ChannelRace(scope, consumeUpstream, n, capacity)

	/**
	 * Run all subsequent operations in a single coroutine (per operation).
	 * Multiple operations can still run concurrently.
	 *
	 * This is equivalent to calling `parallel(1)`.
	 *
	 * @see parallelism
	 * @see parallel
	 */
	fun sequential() = parallel(1)

	//endregion

	fun asChannel(): ReceiveChannel<T> {
		return consumeUpstream
	}

	companion object {

		private const val defaultParallelism = 1
		private const val defaultCapacity = 4

		/**
		 * Creates a [HotRace] from a [ReceiveChannel].
		 *
		 * Each operation will be executed in parallel with [parallelism] actor·s.
		 *
		 * @see raceFrom
		 */
		fun <T> ReceiveChannel<T>.asRace(
			scope: CoroutineScope,
			parallelism: Int = defaultParallelism,
			capacity: Int = defaultCapacity
		) =
			ChannelRace(scope, this, parallelism, capacity)

		/**
		 * Creates a [HotRace] from a [ReceiveChannel].
		 *
		 * Each operation will be executed in parallel with [parallelism] actor·s.
		 *
		 * @see asRace
		 */
		fun <T> CoroutineScope.raceFrom(
			upstream: ReceiveChannel<T>,
			parallelism: Int = defaultParallelism,
			capacity: Int = defaultCapacity
		) =
			ChannelRace(this, upstream, parallelism, capacity)

		/**
		 * Creates a [ChannelRace] from some [items].
		 *
		 * To understand [parallelism] and [capacity], see [ChannelRace].
		 * Although the [parallelism] number is respected by further operations, this function is always sequential (there are no benefits to reading variadic arguments concurrently).
		 */
		fun <T> channelRaceOf(
			scope: CoroutineScope,
			vararg items: T,
			parallelism: Int = defaultParallelism,
			capacity: Int = items.size
		): ChannelRace<T> {
			val channel = Channel<T>(capacity = capacity)

			scope.launch {
				for (item in items)
					channel.send(item)
				channel.close()
			}

			return channel.asRace(scope, parallelism, capacity)
		}

		/**
		 * Creates a [ChannelRace] from an [Iterable].
		 *
		 * To understand the different parameters, see [ChannelRace].
		 * Although [parallelism] is respected by further operations, this function is always sequential.
		 */
		fun <T> Iterable<T>.toChannelRace(
			scope: CoroutineScope,
			parallelism: Int = defaultParallelism,
			capacity: Int = defaultCapacity
		): ChannelRace<T> {
			val data = this
			val channel = Channel<T>(capacity = capacity)

			scope.launch {
				for (item in data)
					channel.send(item)
				channel.close()
			}

			return channel.asRace(scope, parallelism, capacity)
		}

		/**
		 * Creates a [ChannelRace] from a [Collection].
		 *
		 * This is a convenience method for [Iterable.toChannelRace] which uses the [Collection]'s [size][Collection.size] as a default value for the [capacity].
		 */
		fun <T> Collection<T>.toChannelRace(
			scope: CoroutineScope,
			parallelism: Int = defaultParallelism,
			capacity: Int = this.size
		) = (this as Iterable<T>).toChannelRace(scope, parallelism, capacity)

		/**
		 * Creates a [ChannelRace] by consuming another [Race].
		 *
		 * To understand [parallelism] and [capacity], see [ChannelRace].
		 */
		fun <T> Race<T>.toChannelRace(
			scope: CoroutineScope,
			parallelism: Int = defaultParallelism,
			capacity: Int = defaultCapacity
		): ChannelRace<T> {
			val data = this
			val channel = Channel<T>(capacity = capacity)

			val jobs = ArrayList<Job>()

			for (i in 0 until parallelism)
				jobs += scope.launch {
					data.forEach {
						channel.send(it)
					}
				}

			scope.launch {
				jobs.joinAll()
				channel.close()
			}

			return channel.asRace(scope, parallelism = parallelism, capacity = capacity)
		}
	}
}
