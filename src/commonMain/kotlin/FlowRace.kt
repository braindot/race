package fr.braindot.race

import fr.braindot.race.FlowRace.Companion.asRace
import kotlinx.coroutines.flow.*

/**
 * A [ColdRace] implementation using Kotlin's [Flow].
 *
 * Instantiate this class using [asRace].
 */
class FlowRace<T> private constructor(private val upstream: Flow<T>) : ColdRace<T> {

	override fun <U> map(transform: suspend (T) -> U): ColdRace<U> =
		upstream.map(transform).asRace()

	override suspend fun forEach(block: suspend (T) -> Unit) =
		upstream.collect(block)

	fun asFlow() = upstream

	override suspend fun forEachSequential(block: suspend (T) -> Unit) = forEach(block)

	override fun filter(predicate: suspend (T) -> Boolean): ColdRace<T> =
		upstream.filter(predicate).asRace()

	companion object {

		fun <T> Flow<T>.asRace() = FlowRace(this)

		fun <T> flowRaceOf(vararg items: T) = flowOf(*items).asRace()

	}

}
