package fr.braindot.race

/**
 * Common interface for [hot][HotRace] and [cold][ColdRace] streams.
 */
interface Race<T> {

	//region Intermediary operations

	/**
	 * Returns a new race with the same number of elements, but they are all replaced by their respective result from the [transform] function.
	 */
	fun <U> map(transform: suspend (T) -> U): Race<U>

	fun filter(predicate: suspend (T) -> Boolean): Race<T>

	fun filterNot(predicate: suspend (T) -> Boolean): Race<T> = filter { !predicate(it) }

	//endregion
	//region Terminal operations

	/**
	 * Ends this stream by doing an operation on all contents.
	 *
	 * [block] is not guaranteed to be executed sequentially.
	 *
	 * @see forEachSequential
	 */
	suspend fun forEach(block: suspend (T) -> Unit)

	/**
	 * Ends this stream by doing an operation on all contents.
	 *
	 * [block] is guaranteed to be executed sequentially.
	 *
	 * @see forEach
	 */
	suspend fun forEachSequential(block: suspend (T) -> Unit)

	/**
	 * Accumulates values starting with the [initial] value and applying [operation] on the value of the accumulator and each element.
	 */
	suspend fun <R> fold(initial: R, operation: suspend (R, T) -> R): R {
		var result = initial

		forEachSequential {
			result = operation(result, it)
		}

		return result
	}

	/**
	 * Returns `true` if all elements match the given [predicate].
	 *
	 * This operation doesn't guarantee short-circuiting.
	 */
	suspend fun all(predicate: suspend (T) -> Boolean): Boolean = fold(true) { acc, it ->
		acc && predicate(it)
	}

	/**
	 * Returns `true` if at least one element matches the given [predicate].
	 *
	 * This operation doesn't guarantee short-circuiting.
	 */
	suspend fun any(predicate: suspend (T) -> Boolean): Boolean = fold(false) { acc, it ->
		acc || predicate(it)
	}

	/**
	 * Collects all elements from a [Race] and puts them in a [List].
	 *
	 * @param destination Optionally lets the caller select the [List] implementation (the default may change).
	 * @see toSet
	 */
	suspend fun toList(destination: MutableList<T> = ArrayList()): List<T> = toCollection(destination)

	/**
	 * Collects all elements from a [Race] puts them in a [Set].
	 *
	 * @param destination Optionally lets the caller select the [Set] implementation (the default may change).
	 * @see toList
	 */
	suspend fun toSet(destination: MutableSet<T> = HashSet()): Set<T> = toCollection(destination)

	/**
	 * Collects all elements from a [Race] and puts them in a [Collection].
	 *
	 * @param destination The implementation you want to use.
	 * @see toList
	 * @see toSet
	 */
	suspend fun <C: MutableCollection<T>> toCollection(destination: C): C {
		forEachSequential {
			destination += it
		}

		return destination
	}

	//endregion

}

/**
 * A hot stream: each operation starts before the next one requests it.
 */
interface HotRace<T> : Race<T> {

	override fun <U> map(transform: suspend (T) -> U): HotRace<U>

	override fun filter(predicate: suspend (T) -> Boolean): HotRace<T>

	override fun filterNot(predicate: suspend (T) -> Boolean): HotRace<T> =
		super.filterNot(predicate) as HotRace<T>

}

/**
 * A cold stream: each operation starts when the next one requests it.
 */
interface ColdRace<T> : Race<T> {

	override fun <U> map(transform: suspend (T) -> U): ColdRace<U>

	override fun filter(predicate: suspend (T) -> Boolean): ColdRace<T>

	override fun filterNot(predicate: suspend (T) -> Boolean): ColdRace<T> =
		super.filterNot(predicate) as ColdRace<T>

}
