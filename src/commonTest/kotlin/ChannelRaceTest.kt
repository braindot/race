package fr.braindot.race

import fr.braindot.race.ChannelRace.Companion.channelRaceOf
import fr.braindot.race.ChannelRace.Companion.toChannelRace
import fr.braindot.race.FlowRace.Companion.flowRaceOf
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlin.test.Test
import kotlin.test.assertEquals

class ChannelRaceTest {

	private fun newScope() = CoroutineScope(Dispatchers.Default)

	@Test
	fun testInit() = runTest {
		val expected = listOf(1, 2, 3, 4)

		val race = channelRaceOf(newScope(), 1, 2, 3, 4)
		val actual = race.toList()

		assertEquals(expected, actual)
	}

	@Test
	fun testColdToHot() = runTest {
		val expected = listOf(1, 2, 3)

		val actual = flowRaceOf(1, 2, 3)
			.toChannelRace(newScope())
			.toList()

		assertEquals(expected, actual)
	}

	@Test
	fun testMap() = runTest {
		val expected = listOf(1, 16, 4, 36)

		val actual = channelRaceOf(newScope(), 1, 4, 2, 6)
			.map { it * it }
			.toList()

		assertEquals(expected, actual)
	}

	private fun benchmarkMap(generator: (List<Int>) -> ChannelRace<Int>) {
		val data = listOf(1, 456, 156, 237, 98)
		val expected = data.map { it * it }

		val operation: suspend (Int) -> Int = {
			delay(100)
			it * it
		}

		val actual = generator(data).map(operation)

		runTest {
			assertEquals(expected.sorted(), actual.toList().sorted())
		}
	}

	@Test
	fun benchmarkMapSequential() =
		benchmarkMap { it.toChannelRace(newScope(), parallelism = 1) }

	@Test
	fun benchmarkMapParallel() =
		benchmarkMap { it.toChannelRace(newScope(), parallelism = 5) }

	@Test
	fun benchmarkMapSequentialParallel() =
		benchmarkMap { it.toChannelRace(newScope(), parallelism = 1).parallel(5) }

	@Test
	fun benchmarkMapParallelToSequential() =
		benchmarkMap { it.toChannelRace(newScope(), parallelism = 5).sequential() }
}
