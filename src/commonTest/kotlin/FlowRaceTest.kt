package fr.braindot.race

import fr.braindot.race.FlowRace.Companion.asRace
import fr.braindot.race.FlowRace.Companion.flowRaceOf
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

class FlowRaceTest {

	@Test
	fun testSingleton() = runTest {
		val data = flowOf(1)
			.asRace()
			.toList()

		assertEquals(1, data.size)
		assertEquals(1, data[0])
	}

	@Test
	fun testOrder() = runTest {
		val expected = listOf(1, 4, 2, 6)

		val actual = expected.asFlow()
			.asRace()
			.toList()

		assertEquals(expected, actual)
	}

	@Test
	fun testMap() = runTest {
		val start = listOf(1, 4, 2, 6)
		val expected = listOf(1, 16, 4, 36)
		check(start.map { it * it } == expected)

		val actual = start.asFlow()
			.asRace()
			.map { it * it }
			.toList()

		assertEquals(expected, actual)
	}

	@Test
	fun testAsFlow() = runTest {
		val expected = flowOf(1, 2, 3, 4, 5)
		val actual = expected.asRace().asFlow()

		assertSame(expected, actual)
	}

	@Test
	fun testInit() = runTest {
		val expected = flowOf(1, 2, 3, 4, 5).toList()
		val actual = flowRaceOf(1, 2, 3, 4, 5).toList()

		assertEquals(expected, actual)
	}

}
